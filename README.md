# optical_flow
You could use this commands for easier setup on your machine.

# Start with <i>pipenv</i>
From project folder:
```
pipenv install --skip-lock
pipenv run jupyter lab
```

# Start with <i>Docker</i>
From project folder:
```
docker build . -t cv
docker run -p 8888:8888 -v <parent path to>/optical_flow:/optical_flow/ -it cv 
```
